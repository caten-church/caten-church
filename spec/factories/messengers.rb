FactoryBot.define do
  factory :messenger do
    platform "MyString"
    uid "MyString"
    activated false
    user nil
  end
end
