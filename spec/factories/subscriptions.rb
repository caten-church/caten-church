FactoryBot.define do
  factory :subscription do
    new_events false
    joined_event false
    user nil
  end
end
